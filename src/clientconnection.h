
#ifndef __CLIENTCONNECTION_H__
#define __CLIENTCONNECTION_H__

/* Windows Specific Includes */
#include <winsock2.h>
#include <ws2tcpip.h>
#include <process.h>

#include "globaltypes.h"

DWORD WINAPI connectionListener(void *data);
extern volatile bool g_terminateApplication;

#endif /* __CLIENTCONNECTION_H__ */