
#ifndef __HTTPCONNECTION_H__
#define __HTTPCONNECTION_H__

DWORD WINAPI httpConnection(void *data);

typedef enum {METHOD_NOT_IMPLEMENTED = 0, GET} http_method;
typedef enum {HTTP_VERSION_NOT_IMPLEMENTED = 0, HTTP1_1 = 11} http_version;

typedef struct http_uri {
        const char *buffer;
        unsigned short length;
} http_uri_t;

typedef struct http_request_headers {

} http_request_headers_t;

typedef struct http_request {
    http_method method;
    http_uri_t uri;
    http_version version;
} http_request_t;

#endif /* __HTTPCONNECTION_H__ */