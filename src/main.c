/* Main */

/* Standard Includes */
#include <stdio.h>
#include <signal.h>

/* Windows Specific Includes */
#include <windows.h>
#include <process.h>

/* Local Includes */
#include "commandparameters.h"
#include "commandprompt.h"
#include "clientconnection.h"

void interruptHandler(int signal)
{
    g_terminateApplication = true;
    printf("Kill Signal Recieved! Terminating Process...\n");
}

int main(int argc, char *argv[]) 
{
        signal(SIGINT, interruptHandler);
        cliparam_t * const args = getCommandArguments(argc, argv);

        if(args == 0) {
            printf("Invalid arguments! Terminating Process.\n");
            return 1;
        }

        DWORD dwThreadId;
        HANDLE listenerHandle = CreateThread (NULL, 0, *connectionListener, 
                                              (LPVOID)args, 0, &dwThreadId);

        while(g_terminateApplication == false) {
                Sleep(1);
        }


        WaitForSingleObject(listenerHandle, INFINITE);
        

        return 0;
}
