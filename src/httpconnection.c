#include <stdio.h>
#include <string.h>

#include <winsock2.h>
#include <ws2tcpip.h>
#include <process.h>

#include "clientconnection.h"
#include "httpconnection.h"

http_method parseMethod(char *buffer, int size, char **firstSpace, 
                        int *removed) {
    (*firstSpace) = strchr(buffer, ' ') + 1;
    
    (*removed) = ((*firstSpace) - buffer);

    if(0 == strncmp("GET", buffer, (*removed) - 1)) {
        return GET;
    }


    return METHOD_NOT_IMPLEMENTED;
}

http_uri_t parseUri(char *buffer, int size, char **firstSpace, int *removed) 
{
    http_uri_t result;

    (*firstSpace) = strchr(buffer, ' ') + 1;
    (*removed) = (*firstSpace) - buffer;

    result.buffer = buffer;
    result.length = (*removed) - 1;

    return result;
}
http_version parseHttpVersion(char *buffer, int size, char **nextLine, int *removed) 
{
    http_version result;
    (*nextLine) = strchr(buffer, '\r') + 2;

    if(0 == strncmp("HTTP/", buffer, 5)) {
        if(0 == strncmp("1.1", buffer + 5, 3)) {
            printf("HTTP 1.1\n");
            return HTTP1_1;
        }
        printf("Currently only supporting HTTP/1.1\n");
        return HTTP_VERSION_NOT_IMPLEMENTED;
    }
    printf("Undetected HTTP version\n");
    return HTTP_VERSION_NOT_IMPLEMENTED;
}

http_request_t parseRequestLine(char * buffer, int size) {
        
        int removed = 0;

        http_request_t result;

        char* startingPoint = buffer;
        char* nextStartingPoint = NULL;

        int offset;
        result.method = parseMethod(startingPoint, size, &nextStartingPoint, 
                                    &offset);
        removed += offset;
        startingPoint = nextStartingPoint;
        nextStartingPoint = NULL;

        result.uri = parseUri(startingPoint, size, &nextStartingPoint, 
                                    &offset);

        removed += offset;
        startingPoint = nextStartingPoint;
        nextStartingPoint = NULL;

        result.version = parseHttpVersion(startingPoint, size, &nextStartingPoint, 
                                    &offset);

        return result;
}
/*
 * This function is here to seperate out the logic of processing a request from
 * the logic dealing with sockets to make sure I don't accidentally keep a 
 * variable after the buffer it's referring to is out of scope.
 */
void processHttpRequest(char *buffer, int bufferSize) 
{

}

DWORD WINAPI httpConnection(void *data) 
{
        SOCKET skt = (SOCKET)data;
        //Reply to client
        char *message = "HTTP/1.1 200 OK\r\n";
    
        char recvbuf[512];
        http_request_t req;

        while(g_terminateApplication == false) {

                int iResult = recv(skt, recvbuf, 512, 0);
                if ( iResult > 0 ) {
                    printf("Bytes received: %d\n", iResult);
                    printf("%s", recvbuf);
                    req = parseRequestLine(recvbuf, iResult);
                    printf("Method is %d\n", req.method);
                    printf("URI is %.*s\n", req.uri.length, req.uri.buffer);
                    

                    send(skt , message , strlen(message) , 0);

                }
                else if ( iResult == 0 ) {
                    printf("Connection closed\n");
                    break;
                }
                else {
                    int lastError = WSAGetLastError();
                    if(lastError != WSAEWOULDBLOCK ){
                            printf("recv failed: %d\n", lastError);
                    }
                }
                Sleep(1);
        }

        closesocket(skt);

        return 0;
}