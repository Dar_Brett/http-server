/* CommandLine Parameters */


#ifndef __COMMANDPARAMETERS_H__
#define __COMMANDPARAMETERS_H__

typedef struct cliparam {
        unsigned short port;
        unsigned short maxConnections;
} cliparam_t;

cliparam_t * const getCommandArguments(int argc, char *argv[]);


#endif /* __COMMANDPARAMETERS_H__ */