#include <stdio.h>
#include "globaltypes.h"
#include "clientconnection.h"
#include "commandparameters.h"
#include "httpconnection.h"

volatile bool g_terminateApplication = false;

SOCKET initialiseListenSocket() {
        SOCKADDR_IN service;
        WSADATA wsa;
        int result;
        SOCKET listenSocket = INVALID_SOCKET;

        printf("Initialising Server...\n");

        result = WSAStartup(MAKEWORD(2,2), &wsa);
        if(result != 0) {

                printf("Failed to initialise WinSock, Error Code: %2",
                       WSAGetLastError());
                return SOCKET_ERROR;
        }

        listenSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
        if(listenSocket == SOCKET_ERROR) {
                
                printf("Failed to initialise socket, Error Code: %2",
                WSAGetLastError());
                WSACleanup();
                return SOCKET_ERROR;
        }
        unsigned long iMode=1;
        ioctlsocket(listenSocket,FIONBIO,&iMode);
         
        service.sin_family = AF_INET;
        service.sin_addr.s_addr = inet_addr("127.0.0.1");
        service.sin_port = htons(80);

        result = bind(listenSocket, (SOCKADDR *) & service, sizeof (service));
        if (result == SOCKET_ERROR) {
                wprintf(L"bind function failed with error %d\n", 
                        WSAGetLastError());
                
                result = closesocket(listenSocket);
                if (result == SOCKET_ERROR) {
                        wprintf(L"closesocket function failed with error %d\n", 
                                WSAGetLastError());
                }

                WSACleanup();
                return SOCKET_ERROR;
        }

        if (listen(listenSocket, SOMAXCONN) == SOCKET_ERROR) {
                wprintf(L"listen function failed with error: %d\n", 
                        WSAGetLastError());
        }

        wprintf(L"Listening on socket...\n");
        return listenSocket;
}

int firstThreadHolder(HANDLE* arr, unsigned short c) 
{
        int i;
        for (i = 0; i < c; ++i) {
                if(arr[i] == 0)
                        return i;
        }
        return -1;
}

unsigned short checkForClosedThreads(HANDLE* threads, unsigned short c) 
{
        unsigned short closedCount = 0;
        int i;

        for(i = 0; i < c; ++i) {
                if(threads[i] == NULL) {
                        continue;
                }

                if(WAIT_OBJECT_0 == WaitForSingleObject(threads[i], 0)) { 
                    DWORD exitCode; 
                    GetExitCodeThread(threads[i], &exitCode);
                    ++closedCount;
                    threads[i] = NULL;
                }
        }

        return closedCount;
}

DWORD WINAPI connectionListener(void *data)
{
        cliparam_t * const args = (cliparam_t * const)data;
        
        printf("Server port is %d\n", args->port);
        printf("Maximum connections is set to %d\n", args->maxConnections);
        int result;
        int i;

        SOCKET clientSocket;
        SOCKET listenSocket = initialiseListenSocket();

        HANDLE threads[args->maxConnections];
        for(i = 0; i < args->maxConnections; ++i) {
            threads[i] = NULL;
        }

        wprintf(L"Waiting for client to connect...\n");



        printf( "Accepting Connections.\n");
        SOCKET AcceptSocket;
        unsigned short numberOfConnections = 0;
        while(g_terminateApplication == false) {
                int closed = checkForClosedThreads(threads, 
                                                   args->maxConnections);
                if(closed > 0) {
                    printf("%d connections were closed.\n", closed);
                    numberOfConnections -= closed;
                }
                AcceptSocket = accept(listenSocket, NULL, NULL);
                
                if(AcceptSocket == SOCKET_ERROR) {
                    Sleep(1);
                    continue;
                } 

                if(numberOfConnections == args->maxConnections) {
                    printf("Maximum connections reached\n");
                    continue;
                }

                DWORD dwThreadId;

                int index = firstThreadHolder(threads, args->maxConnections);

                threads[index] = CreateThread (NULL, 0, &httpConnection, 
                                               (LPVOID) AcceptSocket, 0, 
                                               &dwThreadId);
                ++numberOfConnections;
                printf( "Total of %d connected users.\n", numberOfConnections);
                AcceptSocket = SOCKET_ERROR;
        }

        result = closesocket(listenSocket);
        if (result == SOCKET_ERROR) {
                wprintf(L"closesocket function failed with error %d\n",
                            WSAGetLastError());

                WSACleanup();
                return 1;
        }

        for(i = 0; i < args->maxConnections; ++i) {
                if(threads[i] == 0) {
                        continue;
                }
                WaitForSingleObject(threads[i], INFINITE);
        }

        printf("Shutting down Server...\n");
        WSACleanup();
}