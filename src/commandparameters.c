/* Standard Includes */
#include <string.h>
#include <stdio.h>
#include <errno.h>

#include "globaltypes.h"
#include "commandparameters.h"

/* 
 * Anythign starting with a single dash '-' will be treated as an alias for a 
 * lookup of the full length version of that OPTION.
 * 
 * FLAGS differ only in the sense that they are binary, so they're false if 
 * the are not specified.
 * 
 * All other OPTIONS will have a default value specified, which will be 
 * overridden if it is specified via the arguments.
 *
 * If an OPTION is expecting a value then the next argument will be considered
 * the value of that OPTION. Except for in the cases of a string to be displayed
 * to users, or used to interact with an external system.
 */

typedef enum {OPTION, OPTIONVALUE, FLAG} parameterType;

typedef enum {INVALID_OPTION = 0, PORT} option;


cliparam_t __hiddenParameters;

void setDefaultParameters() {
        __hiddenParameters.port = 80;
        __hiddenParameters.maxConnections = 8;
}

option getOption(char *argc)
{
        size_t len = strlen(argc);


        if(len < 2 || argc[0] != '-') {
                return INVALID_OPTION;
        }
        
        if (strcmp(argc, "-p") == 0 || strcmp(argc, "--port") == 0) {
                return PORT;
        }

        return INVALID_OPTION;
}
bool optionNeedsValue(option opt) {
        switch(opt) {
                case PORT:
                        return true;
                default:
                        return false;
        }
}
bool setParameter(option opt, char *value) {
        if(opt == PORT) {
                char *ptr;
                errno = 0;
                long ret = strtol(value, &ptr, 10);
                if(ptr == value) {
                        printf("Non-numeric string in port parameter\n");
                        return false;
                }
                if(errno != 0)
                {
                        printf("Error Parsing Port: %s\n", strerror(errno));
                        return false;
                }
                if(65535 < ret) {
                        printf("Max port number is 65535 specified was %d\n", 
                                ret);
                        return false;
                }
                __hiddenParameters.port = ret;
                return true;
        }
        return false;
}


cliparam_t * const getCommandArguments(int argc, char *argv[])
{
        setDefaultParameters();

        int i;
        /* Intentionally skipping 0 since that's the name of the executable */
        for(i = 1; i < argc; ++i)
        {
                option opt = getOption(argv[i]);
                if(opt == INVALID_OPTION) {
                        printf("%s is not a valid option!\n", argv[i]);
                        return 0;
                }
                bool valueNeeded = optionNeedsValue(opt);
                if(valueNeeded && i+1 == argc){
                        printf("Missing option value for %s\n", argv[i]);
                        return false;
                }
                if(valueNeeded)
                {
                        if(setParameter(opt, argv[++i]) == false){
                                return false;
                        }
                        /* Rethink this if further calcuations are needed */
                        continue;
                }
        }
        return &__hiddenParameters;
}


